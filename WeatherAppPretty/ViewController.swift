//
//  ViewController.swift
//  WeatherAppPretty
//
//  Created by robin on 2018-07-15.
//  Copyright © 2018 robin. All rights reserved.
//

import UIKit
import Alamofire            // 1. Import AlamoFire and SwiftyJson
import SwiftyJSON

import ChameleonFramework         // optional - used to make colors pretty

import CoreLocation               // TODO: Add location later


class ViewController: UIViewController, CLLocationManagerDelegate {

    // --------------------------------------------
    // MARK: User interface outlets
    // --------------------------------------------
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTemp: UILabel!
    @IBOutlet weak var imageWeatherIcon: UIImageView!
    @IBOutlet weak var labelHumidity: UILabel!
    @IBOutlet weak var labelVisibility: UILabel!
    @IBOutlet weak var labelRain: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    
    // --------------------------------------------
    // TODO: url variables
    // --------------------------------------------
    let URL = "https://api.darksky.net/forecast/ff41689fc242d7783a79fab7ae586b2b/"
    let LOCATION = "28.7041,77.1025"
    let PARAMS = "?exclude=minutely,hourly,daily,alerts,flags&units=ca"

    
    // --------------------------------------------
    // TODO: Add location variables
    // --------------------------------------------
    
    // put your location variables here
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // UI NONSENSE - Make a gradient background using Chameleon
        let colors:[UIColor] = [
            UIColor(hexString:"#F27121")!,
            UIColor(hexString:"#E94057")!,
            UIColor(hexString:"#8A2387")!
        ]
        view.backgroundColor = UIColor(gradientStyle:UIGradientStyle.topToBottom, withFrame:view.frame, andColors:colors)

        
        // TODO: Get the current location
        //  do this later
        
        
        // TODO: Get the weather
        getWeather(url:URL)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func refreshWeather(_ sender: Any) {
        // TODO: Refresh the weather data.
        print("refresh weather button pushed")
    }

    
    func getWeather(url:String) {
        // Build the URL:
        
        
        let url = "https://dog.ceo/api/breeds/image/random"
        print(url)
        
        
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON {
            
            (response) in
            
            if response.result.isSuccess {
                if let dataFromServer = response.data {
                    
        
                    do {
                        let json = try JSON(data: dataFromServer)
                        
                        // TODO: Parse the json response
                        print(json)
                        
                        // TODO: Display the results
                        let s = json["status"].stringValue
                        let m = json["message"].stringValue
                        
                        print(s)
                        print(m)
                        
                    }
                    catch {
                        print("error")
                    }
                    
                }
                else {
                    print("Error when getting JSON from the response")
                }
            }
            else {
                // 6. You got an error while trying to connect to the API
                print("Error while fetching data from URL")
            }
            
        }
        

    }
    
    
    // --------------------------------------------
    // TODO: Add location
    // --------------------------------------------
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // this function gets called every time the person's location changes
        print("location updated!")

        
        
    }
    

}

